import re

text_to_search = '''
* Test title of note :tag1:tag2:ideas2:
test
** TODO Test title of note :tag1:tag2:ideas:hi:
* DONE Hi
'''

note_pattern = re.compile(r'\*+ (.*)\n?')

notes = note_pattern.finditer(text_to_search)

for note in notes:
    # Get the current match's string
    print(note.group(0))

    # Extract from the note's text only the tags,
    # between the colons
    tags_raw = re.search(r':(.*):', note.group(1))
    
    # 'None' is raised if the search above finds nothing
    # Check for case when None is encountered
    if tags_raw != None:
        # Split string to get individual tags
        # .group(1) is used to grab the group in pattern
        tags = re.split(':', tags_raw.group(1))

        # # Prints the list of tags
        # print(tags)

        for tag in tags:
            print(tag)